# Bonus-track final project



## Описание

Репозиторий для финального проекта курса по проф. переподготовке Bonus-track "Технологии анализа данных". Сдача планируется в июне 2024 года. Проект не в финальном состоянии и будет дорабатываться.

В качестве задачи была выбрана простановка тэгов/жанров книгам на основе текстового описания, то есть многоклассовая (multilabel) классификация. Ввиду отсутствия опыта работы с рекуррентными нейронными сетями, которые подошли бы для этой задачи лучше, было решено использовать CatBoostClassifier.

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/KamilFathiev/bonus-track-final-project.git
git branch -M main
git push -uf origin main
```
